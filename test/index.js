
const fs = require('fs');
const assert = require('assert');

// eslint-disable-next-line
const htmlTempltes = require('../libs/htmlTempltes');

describe('Check template processor', function() {
  
  it('recursive includes', function() {
    
    let src = include(process.cwd() + '/test/html_src/pages/index.html');
    let test = fs.readFileSync(process.cwd() + '/test/index.html', { encoding: 'utf8' });
    
    assert.equal(src, test);
  });
  
  it('variables', function() {
    
    // eslint-disable-next-line
    test_var = 'ololo';
    
    let src = include(process.cwd() + '/test/html_src/pages/vars.html');
    let test = fs.readFileSync(process.cwd() + '/test/vars.html', { encoding: 'utf8' });
    
    assert.equal(src, test);
  });
  
});
