# simple-html-templates


This is simple html template compiler. It provides "include .html" function and standard es6 variable templates. It has 0 dependencies.

## Installation


```
npm i simple-html-templates
```

## Cli usage


Process directory

```
sht-cli src_dir dst_dir [recursive]
```

or single file

```
sht-cli ./src/index.html ./pub/index.html
```


## Template usage


### function include(relative_file_path)


```
...

<div class="body">${include('../modules/body.html')}</div>
...
```

"include" works with paths like standard "require" function.


### variables


You can use standard es6 templates to get variable value, and "set" function to set global variable.

```
// define test variable inline
${set('test', 'world')}
<p>Hello ${test}</p>
```

Also you can use "get" function.

```
// define test variable inline
${set('test', 'world')}
<p>Hello ${get('test')}</p>
```

### Examples 


You can find more examples here: https://gitlab.com/hololoev/simple-html-templates/tree/master/examples


## Integrating to package.json


```
"scripts": {
  "build": "sht-cli ./src ./pub recursive"
},
```
